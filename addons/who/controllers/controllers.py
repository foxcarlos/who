# -*- coding: utf-8 -*-
from odoo import http

class Who(http.Controller):
    @http.route('/who/', auth='public')
    def index(self, **kw):
        return "Hola, Mundo"

#     @http.route('/who/who/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('who.listing', {
#             'root': '/who/who',
#             'objects': http.request.env['who.who'].search([]),
#         })

#     @http.route('/who/who/objects/<model("who.who"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('who.object', {
#             'object': obj
#         })
