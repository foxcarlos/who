# -*- coding: utf-8 -*-
# pylint: disable=all

from odoo import http

class Intranet(http.Controller):
    '''.'''


    @http.route('/prueba_post/', auth='public', website=True, csrf=False)
    def post(self, **kw):
        import json

        json_pasado = kw
        return json.dumps(json_pasado)

    @http.route('/intranet/', auth='public', website=True)
    def index(self, **kw):
        '''.'''
        # Buscar en el models
        Personas = http.request.env['persona']
        # Armo el json o Dict
        params = {"personas": Personas.search([])}
        return http.request.render('registro.index', params)

    @http.route('/intranet/<name>/', auth='public', website=True)
    def nombre(self, name):
        '''.'''

        html_li = ""
        html_final = ""
        response = ''

        var_personas = http.request.env['persona'].search(
                [('nombres', 'ilike', name)])

        if var_personas:
            for persona in var_personas:
                # html_li += "<li>{0} {1}</li>".format(persona.nombres,
                #        persona.apellidos)
                html_li += """
                <tr>
                    <td align="center">
                        <a class="btn btn-default"><em class="fa fa-pencil"></em></a>
                    </td>
                    <td>{0}</td>
                    <td>{1}</td>
                </tr>""".format(persona.nombres, persona.apellidos)

            html_final = html_li  # "<ul>{0}</ul>".format(html_li)
            return html_final
        else:
            response = "<h1>Lo siento la persona:{0} no existe</h1>".format(name)

        return response

    @http.route('/intranet/<int:id>/', auth='public', website=True)
    def id(self, id):
        '''.'''
        response = {'id': id}
        import json
        return json.dumps(response)

    @http.route('/intranet/biografia/<model("persona"):nombre>/', auth='public', website=True)
    def biografia(self,nombre):
        params = {'person': nombre}
        return http.request.render('registro.biografia', params)
