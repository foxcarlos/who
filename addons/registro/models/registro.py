# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
# Generated by the Odoo plugin for Dia !
from openerp import api, fields, models
from odoo.exceptions import ValidationError, UserError


class Persona(models.Model):
    """Model para el registro de personas"""
    _description = 'Models de personas'
    _name = 'persona'
    _rec_name = 'cedula'
    _order = 'apellidos desc'

    cedula = fields.Char(string='Cedula',
                         help='Ingrese la cedula con el formato V99999999')
    nombres = fields.Char(string='Nombres',
                          help='Ingrese primer nombre y segundo de la persona')
    apellidos = fields.Char(string='Apellidos',
                            help='Ingrese el primer y segundo apellido')
    direccion = fields.Text(string='Direccion',
                            help='Ingrese la direccion de habitacion')
    sexo = fields.Selection([('M', 'Masculino'), ('F', 'Femenino')],
                            string='Sexo', help='Elija el Sexo')
    fecha_nacimiento = fields.Date(string="Fecha de Nacimiento",
                                   help="Elija la fecha de Nacimiento en formato DD/MM/AAAA")
    ingreso_mensual = fields.Float(string="Ingreso Mensual",
                                   help="Indique su ingreso Mensual")
    mensaje = fields.Html(string="Mensaje", help="Indique un mensaje")
    active = fields.Boolean(string="Activo",
                            help="Activar o desactivar registro")
    foto = fields.Binary(string="Fotografia", help="Seleccione una foto")
    state_id = fields.Many2one('res.country.state', string='Estado')
    municipality_id = fields.Many2one('res.country.state.municipality',
                                      string='Municipio')
    cargo_id = fields.Many2one('persona.cargo', string="Cargo")
    telefono_ids = fields.One2many('persona.telefono',
                                   'persona_id', string='Telefonos')
    deporte_ids = fields.Many2many('persona.deporte', string='Deportes')

    _sql_constraints = [('cedula_uniq', 'UNIQUE(cedula)',
                         'La cedula debe ser unica')]

    # Se pueden hacer contraints compuestos
    # _sql_constraints = [
    #    ('cedula_uniq','UNIQUE(cedula)' , 'La cedula debe ser unica'),
    #   ('compuestos_uniq','UNIQUE(nombres, sexo)' , 'Ya existe una persona que
    # tiene el nombre y el sexo'),
    #    ]

    """
    @api.model
    def name_get(self):
        result = []
        for persona in self:
            name = u"{0}-{1} {2}".format(persona.cedula, persona.nombres,
                                         persona.apellidos)
            result.append((persona.id, name))
        return result"""

    @api.multi
    def probar_metodo(self):
        '''.'''
        for person in self:
            buscar = person.search([('nombres', 'ilike', 'carl')])
        raise ValidationError([f.nombres for f in buscar])

    @api.multi
    def desactivar(self):
        '''.'''
        for record in self:
            record.active = False

    @api.constrains("cargo_id")
    def _onchange_cargo(self):
        if self.cargo_id.cargo.lower() == 'funcional':
            raise ValidationError('Es un Groso no puedes guardarlo')

    @api.onchange("nombres")
    def _onchange_nombres(self):
        if self.nombres == 'carlos':
            raise ValidationError('Lindo Nombre')


    @api.constrains("fecha_nacimiento")
    def _valida_fecha_nacimiento(self):
        if self.fecha_nacimiento > fields.Datetime.now():
            raise ValidationError('La fecha de nacimiento "{}" no debe ser\
                                  mayor a la fecha  de hoy "{}" '.format(
                                      self.fecha_nacimiento,
                                      fields.Datetime.now()))

class PersonaCargo(models.Model):
    """."""
    _description = 'Models de Cargos'
    _name = 'persona.cargo'
    _rec_name = "cargo"
    _order = "cargo"

    cargo = fields.Char(string='Cargo', help="Cargo")
    descripcion = fields.Char('Descripcion del Cargo')

    _sql_constraints = [('cargo_uniq', 'UNIQUE(cargo)',
                         'El Cargo debe ser unico')]


class PersonaTelefono(models.Model):
    """."""
    _description = 'Models de Telefonos'
    _name = 'persona.telefono'

    telefono = fields.Char(string='Telefono')
    persona_id = fields.Many2one('persona')

class PersonaDeporte(models.Model):
    """."""

    _description = 'Models de Deportes'
    _name = 'persona.deporte'

    name = fields.Char(string='Deporte')
    description = fields.Text(string='Descripcion')
