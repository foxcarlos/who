# Modulos Odoo para Who delivery

Todos los Moculos hechos para Who Delivery.

  - Usa Celery y Redis para ejecutar procesos en background para AFIP
  

### Installation

Odoo10 Who requires python v2 to run.
```sh
$ sudo apt-get install python2.7 python2.7-doc
```


= Repo =
```sh
copia desde tu pc cat ~/.ssh/id_rsa.pub y pegalo
en tu setting https://gitlab.com/profile/keys

Luego en tu PC clonas el repositorio:
$ git@gitlab.com:foxcarlos/cotesma_addons.git

```

= Prepare Virtual Env =
```sh
$ mkdir ~/Envs
$ sudo pip install virtualenvwrapper
$ sudo apt-get install python-virtualenv

Note:Edit File in your Home
$ vim ~.bashrc
export WORKON_HOME=~/Envs
source /usr/local/bin/virtualenvwrapper.sh

Run in terminal
$ mkvirtualenv --python=/usr/bin/python3 para_python3
$ mkvirtualenv para_python2
```

= Install the dependencies =
```sh
$ pip install -r requirements.txt
```

### Todos

 - Write MORE Tests
 - Add Night Mode

### readme.md Creado con https://dillinger.io/
